/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "net_policy_service_stub.h"

#include "net_mgr_log_wrapper.h"
#include "net_policy_core.h"
#include "net_quota_policy.h"
#include "netmanager_base_permission.h"

namespace OHOS {
namespace NetManagerStandard {
NetPolicyServiceStub::NetPolicyServiceStub()
{
    memberFuncMap_[CMD_NPS_SET_POLICY_BY_UID] = &NetPolicyServiceStub::OnSetPolicyByUid;
    memberFuncMap_[CMD_NPS_GET_POLICY_BY_UID] = &NetPolicyServiceStub::OnGetPolicyByUid;
    memberFuncMap_[CMD_NPS_GET_UIDS_BY_UID] = &NetPolicyServiceStub::OnGetUidsByPolicy;
    memberFuncMap_[CMD_NPS_IS_NET_ALLOWED_BY_METERED] = &NetPolicyServiceStub::OnIsUidNetAllowedMetered;
    memberFuncMap_[CMD_NPS_IS_NET_ALLOWED_BY_IFACE] = &NetPolicyServiceStub::OnIsUidNetAllowedIfaceName;
    memberFuncMap_[CMD_NPS_REGISTER_NET_POLICY_CALLBACK] = &NetPolicyServiceStub::OnRegisterNetPolicyCallback;
    memberFuncMap_[CMD_NPS_UNREGISTER_NET_POLICY_CALLBACK] = &NetPolicyServiceStub::OnUnregisterNetPolicyCallback;
    memberFuncMap_[CMD_NPS_SET_NET_QUOTA_POLICIES] = &NetPolicyServiceStub::OnSetNetQuotaPolicies;
    memberFuncMap_[CMD_NPS_GET_NET_QUOTA_POLICIES] = &NetPolicyServiceStub::OnGetNetQuotaPolicies;
    memberFuncMap_[CMD_NPS_RESET_POLICIES] = &NetPolicyServiceStub::OnResetPolicies;
    memberFuncMap_[CMD_NPS_UPDATE_REMIND_POLICY] = &NetPolicyServiceStub::OnSnoozePolicy;
    memberFuncMap_[CMD_NPS_SET_IDLE_ALLOWED_LIST] = &NetPolicyServiceStub::OnSetDeviceIdleAllowedList;
    memberFuncMap_[CMD_NPS_GET_IDLE_ALLOWED_LIST] = &NetPolicyServiceStub::OnGetDeviceIdleAllowedList;
    memberFuncMap_[CMD_NPS_SET_DEVICE_IDLE_POLICY] = &NetPolicyServiceStub::OnSetDeviceIdlePolicy;
    memberFuncMap_[CMD_NPS_SET_BACKGROUND_POLICY] = &NetPolicyServiceStub::OnSetBackgroundPolicy;
    memberFuncMap_[CMD_NPS_GET_BACKGROUND_POLICY] = &NetPolicyServiceStub::OnGetBackgroundPolicy;
    memberFuncMap_[CMD_NPS_GET_BACKGROUND_POLICY_BY_UID] = &NetPolicyServiceStub::OnGetBackgroundPolicyByUid;
    memberFuncMap_[CMD_NPS_GET_BACKGROUND_POLICY_BY_CURRENT] = &NetPolicyServiceStub::OnGetCurrentBackgroundPolicy;
    InitEventHandler();
}

NetPolicyServiceStub::~NetPolicyServiceStub() = default;

void NetPolicyServiceStub::InitEventHandler()
{
    runner_ = AppExecFwk::EventRunner::Create(NET_POLICY_WORK_THREAD);
    if (!runner_) {
        NETMGR_LOG_E("Create net policy work event runner.");
        return;
    }
    auto core = DelayedSingleton<NetPolicyCore>::GetInstance();
    handler_ = std::make_shared<NetPolicyEventHandler>(runner_, core);
    core->Init(handler_);
}

int32_t NetPolicyServiceStub::OnRemoteRequest(uint32_t code,
                                              MessageParcel &data,
                                              MessageParcel &reply,
                                              MessageOption &option)
{
    std::u16string myDescriptor = NetPolicyServiceStub::GetDescriptor();
    std::u16string remoteDescriptor = data.ReadInterfaceToken();
    if (myDescriptor != remoteDescriptor) {
        NETMGR_LOG_E("descriptor checked fail");
        return ERR_FLATTEN_OBJECT;
    }

    if (handler_ == nullptr) {
        NETMGR_LOG_E("Net policy handler is null, re-create handler.");
        InitEventHandler();
    }

    auto itFunc = memberFuncMap_.find(code);
    int32_t result = ERR_NONE;
    if (itFunc != memberFuncMap_.end()) {
        auto requestFunc = itFunc->second;
        if (requestFunc != nullptr) {
            handler_->PostSyncTask(
                [this, &data, &reply, &requestFunc, &result]() { result = (this->*requestFunc)(data, reply); },
                AppExecFwk::EventQueue::Priority::HIGH);
            return result;
        }
    }
    return IPCObjectStub::OnRemoteRequest(code, data, reply, option);
}

bool NetPolicyServiceStub::CheckPermission(const std::string &permission, const std::string &funcName)
{
    if (NetManagerPermission::CheckPermission(permission)) {
        return true;
    }
    NETMGR_LOG_E("Permission denied function: %{public}s permission: %{public}s", funcName.c_str(), permission.c_str());
    return false;
}

int32_t NetPolicyServiceStub::OnSetPolicyByUid(MessageParcel &data, MessageParcel &reply)
{
    uint32_t uid;
    if (!data.ReadUint32(uid)) {
        return ERR_FLATTEN_OBJECT;
    }

    uint32_t netPolicy;
    if (!data.ReadUint32(netPolicy)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteInt32(static_cast<int32_t>(SetPolicyByUid(uid, static_cast<NetUidPolicy>(netPolicy))))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnGetPolicyByUid(MessageParcel &data, MessageParcel &reply)
{
    uint32_t uid;
    if (!data.ReadUint32(uid)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteInt32(static_cast<int32_t>(GetPolicyByUid(uid)))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnGetUidsByPolicy(MessageParcel &data, MessageParcel &reply)
{
    uint32_t policy;
    if (!data.ReadUint32(policy)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteUInt32Vector(GetUidsByPolicy(static_cast<NetUidPolicy>(policy)))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnIsUidNetAllowedMetered(MessageParcel &data, MessageParcel &reply)
{
    uint32_t uid = 0;
    bool metered = false;
    if (!data.ReadUint32(uid)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!data.ReadBool(metered)) {
        return ERR_FLATTEN_OBJECT;
    }

    bool ret = IsUidNetAllowed(uid, metered);
    if (!reply.WriteBool(ret)) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnIsUidNetAllowedIfaceName(MessageParcel &data, MessageParcel &reply)
{
    uint32_t uid = 0;
    std::string ifaceName;
    if (!data.ReadUint32(uid)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!data.ReadString(ifaceName)) {
        return ERR_FLATTEN_OBJECT;
    }
    bool ret = IsUidNetAllowed(uid, ifaceName);
    if (!reply.WriteBool(ret)) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnRegisterNetPolicyCallback(MessageParcel &data, MessageParcel &reply)
{
    int32_t result = ERR_FLATTEN_OBJECT;
    sptr<IRemoteObject> remote = data.ReadRemoteObject();
    if (remote == nullptr) {
        NETMGR_LOG_E("Callback ptr is nullptr.");
        reply.WriteInt32(result);
        return result;
    }

    sptr<INetPolicyCallback> callback = iface_cast<INetPolicyCallback>(remote);
    result = RegisterNetPolicyCallback(callback);
    reply.WriteInt32(result);
    return result;
}

int32_t NetPolicyServiceStub::OnUnregisterNetPolicyCallback(MessageParcel &data, MessageParcel &reply)
{
    int32_t result = ERR_FLATTEN_OBJECT;
    sptr<IRemoteObject> remote = data.ReadRemoteObject();
    if (remote == nullptr) {
        NETMGR_LOG_E("callback ptr is nullptr.");
        reply.WriteInt32(result);
        return result;
    }
    sptr<INetPolicyCallback> callback = iface_cast<INetPolicyCallback>(remote);
    result = UnregisterNetPolicyCallback(callback);
    reply.WriteInt32(result);
    return result;
}

int32_t NetPolicyServiceStub::OnSetNetQuotaPolicies(MessageParcel &data, MessageParcel &reply)
{
    std::vector<NetQuotaPolicy> quotaPolicies;
    if (!NetQuotaPolicy::Unmarshalling(data, quotaPolicies)) {
        NETMGR_LOG_E("Unmarshalling failed.");
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteInt32(static_cast<int32_t>(SetNetQuotaPolicies(quotaPolicies)))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnGetNetQuotaPolicies(MessageParcel &data, MessageParcel &reply)
{
    std::vector<NetQuotaPolicy> quotaPolicies;

    if (GetNetQuotaPolicies(quotaPolicies) != NetPolicyResultCode::ERR_NONE) {
        NETMGR_LOG_E("GetNetQuotaPolicies failed.");
        return ERR_FLATTEN_OBJECT;
    }

    if (!NetQuotaPolicy::Marshalling(reply, quotaPolicies)) {
        NETMGR_LOG_E("Marshalling failed");
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnResetPolicies(MessageParcel &data, MessageParcel &reply)
{
    std::string subscrberId;
    if (!data.ReadString(subscrberId)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteInt32(static_cast<int32_t>(ResetPolicies(subscrberId)))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnSetBackgroundPolicy(MessageParcel &data, MessageParcel &reply)
{
    bool isBackgroundPolicyAllow = false;
    if (!data.ReadBool(isBackgroundPolicyAllow)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteInt32(static_cast<int32_t>(SetBackgroundPolicy(isBackgroundPolicyAllow)))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnGetBackgroundPolicy(MessageParcel &data, MessageParcel &reply)
{
    bool ret = GetBackgroundPolicy();
    if (!reply.WriteBool(ret)) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnGetBackgroundPolicyByUid(MessageParcel &data, MessageParcel &reply)
{
    uint32_t uid = 0;
    if (!data.ReadUint32(uid)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteUint32(GetBackgroundPolicyByUid(uid))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnGetCurrentBackgroundPolicy(MessageParcel &data, MessageParcel &reply)
{
    if (!reply.WriteInt32(static_cast<int32_t>(GetCurrentBackgroundPolicy()))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnSnoozePolicy(MessageParcel &data, MessageParcel &reply)
{
    int32_t netType = 0;
    if (!data.ReadInt32(netType)) {
        return ERR_FLATTEN_OBJECT;
    }

    std::string iccid;
    if (!data.ReadString(iccid)) {
        return ERR_FLATTEN_OBJECT;
    }

    uint32_t remindType = 0;
    if (!data.ReadUint32(remindType)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteInt32(static_cast<int32_t>(UpdateRemindPolicy(netType, iccid, remindType)))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnSetDeviceIdleAllowedList(MessageParcel &data, MessageParcel &reply)
{
    uint32_t uid;
    if (!data.ReadUint32(uid)) {
        return ERR_FLATTEN_OBJECT;
    }

    bool isAllowed = false;
    if (!data.ReadBool(isAllowed)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteInt32(static_cast<int32_t>(SetDeviceIdleAllowedList(uid, isAllowed)))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnGetDeviceIdleAllowedList(MessageParcel &data, MessageParcel &reply)
{
    std::vector<uint32_t> uids;
    if (GetDeviceIdleAllowedList(uids) != NetPolicyResultCode::ERR_NONE) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteUInt32Vector(uids)) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}

int32_t NetPolicyServiceStub::OnSetDeviceIdlePolicy(MessageParcel &data, MessageParcel &reply)
{
    bool isAllowed = false;
    if (!data.ReadBool(isAllowed)) {
        return ERR_FLATTEN_OBJECT;
    }

    if (!reply.WriteInt32(SetDeviceIdlePolicy(isAllowed))) {
        return ERR_FLATTEN_OBJECT;
    }

    return ERR_NONE;
}
} // namespace NetManagerStandard
} // namespace OHOS
